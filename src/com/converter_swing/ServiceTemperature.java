package com.converter_swing;

public class ServiceTemperature {

    /** C <--> K (Шкала Кельвина)*/
    /** C <--> F (Шкала Фаренгейта)*/
    /** C <--> Re (Шкала Реомюра) */
    /** C <--> Ro (Шкала Рёмер)*/
    /** C <--> Ra (Шкала Ранкина)*/
    /** C <--> N (Шкала Ньютона)*/
    /** C <--> D (Шкала Дели́ля)*/

    double C = 0;
    double K = 0;
    double F = 0;
    double Re = 0;
    double Ro = 0;
    double Ra = 0;
    double N = 0;
    double D = 0;


    public double convertCelsiusIntoKelvin(double C) {
        return K = C + 273.15;
    }
    public double convertCelsiusIntoFahrenheit(double C) {
        return F = (C * 1.8) + 32;
    }

    public double convertCelsiusIntoReaumur(double C) {
        return Re = C * 0.8;
    }

    public double convertCelsiusIntoRomer(double C) {
        return Ro = (C * 0.525) +7.5;
    }

    public double convertCelsiusIntoRankine(double C) {
        return Ra = (C + 273.15) * 1.8;
    }

    public double convertCelsiusIntNewton(double C) {
        return N = C * 0.33;
    }

    public double convertCelsiusIntoDelisle(double C) {
        return D = (100 - C) * 0.333;
    }

    //////////////////////////////////////////////

    public double convertKelvinIntoCelsius(double K) {
        return C = K - 273.15;
    }
    public double convertFahrenheitIntoCelsius(double F) {
        return C = (F -32) /1.8;
    }

    public double convertReaumurIntoCelsius(double Re) {
        return C = Re / 0.8;
    }

    public double convertRomerIntoCelsius(double Ro) {
        return C = (Ro - 7.5) * 1.904;
    }

    public double convertRankineeIntoCelsius(double Ra) {
        return C = (Ra - 491.67) * 0.555;
    }

    public double convertNewtonIntoCelsius(double N) {
        return C = N * 3.03;
    }

    public double convertDelisleIntoCelsius(double D) {
        return C = (100 - D) * 0.666;
    }
}
