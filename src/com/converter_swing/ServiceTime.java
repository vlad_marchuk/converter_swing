package com.converter_swing;

public class ServiceTime {

    double sec = 0;
    double min = 0;
    double hour = 0;
    double day = 0;
    double week = 0;
    double month = 0;
    double astronomicalYear  = 0;

    public double convertSecIntoMin(double sec) {
        return min = sec / 60;
    }

    public double convertSecIntoHour(double sec) {
        return hour = sec / 3600;
    }
    public double convertSecIntoDay(double sec) {
        return day = sec / 86400;
    }

    public double convertSecIntoWeek(double sec) {
        return week = sec / 604800;
    }

    public double convertSecIntoMonth(double sec) {
        return month = sec / 2592000 ;
    }
    public double convertSecIntoAstronomicalYear(double sec) {
        return astronomicalYear = sec / 31536000;
    }

    public double convertMinIntoSec(double min) {
        return sec = min * 60;
    }

    public double convertHourIntoSec(double hour) {
        return sec = hour * 3600;
    }

    public double convertDayIntoSec(double day) {
        return sec = day * 86400;
    }

    public double convertWeekIntoSec(double week) {
        return sec = week * 604800;
    }

    public double convertMonthIntoSec(double month) {
        return sec = month * 2592000 ;
    }

    public double convertAstronomicalYearIntoSec(double astronomicalYear) {
        return sec = astronomicalYear * 31536000;
    }

}
