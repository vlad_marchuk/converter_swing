package com.converter_swing;

public class ServiceVolume {

    double l = 0;
    double m3 = 0;
    double gallon = 0;
    double pint = 0;
    double quart = 0;
    double barrel = 0;
    double cubicFoot = 0;

    public double convertLiterIntoM(double l) {
        return m3 = l * 0.001;
    }

    public double convertLiterIntoGallon(double l) {
        return gallon = l * 0.2642;
    }

    public double convertLiterIntoPint(double l) {
        return pint = l * 2.113;
    }

    public double convertLiterIntoQuart(double l) {
        return quart = l * 1.057;
    }

    public double convertLiterIntoBarrel(double l) {
        return barrel = l * 0.00629;
    }

    public double convertLiterIntoCubicFoot(double l) {
        return cubicFoot = l * 0.03531;
    }
    //////////////////////////////////////////////

    public double convertMIntoLiter(double m3) {
        return l = m3 / 0.001;
    }

    public double convertGallonIntoLiter(double gallon) {
        return l = gallon / 0.2642;
    }

    public double convertPintIntoLiter(double pint) {
        return l = pint / 2.113;
    }

    public double convertQuartIntoLiter(double quart) {
        return l = quart / 1.057;
    }

    public double convertBarrelIntoLiter(double barrel) {
        return l = barrel / 0.00629;
    }

    public double convertCubicFootIntoLiter(double cubicFoot) {
        return l = cubicFoot / 0.03531;
    }
}
