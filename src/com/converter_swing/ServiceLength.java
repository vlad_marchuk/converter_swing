package com.converter_swing;

public class ServiceLength {

    double m = 0;
    double km = 0;
    double mile = 0;
    double nauticalMile = 0;
    double cable = 0;
    double league = 0;
    double foot = 0;
    double yard = 0;


    public double convertMeterIntoKilometre(double m) {
        return km = m * 0.001;
    }

    public double convertMeterIntoMile(double m) {
        return mile = m * 0.0006214;
    }

    public double convertMeterIntoNauticalMile(double m) {
        return nauticalMile = m * 0.00054;
    }

    public double convertMeterIntoCable(double m) {
        return cable = m * 0.0054;
    }

    public double convertMeterIntoLeague(double m) {
        return league = m * 0.00018;
    }

    public double convertMeterIntoFoot(double m) {
        return foot = m * 3.281;
    }
    public double convertMeterIntoYard(double m) {
        return yard = m * 1.094;
    }
    //////////////////////////////////////////////

    public double convertKilometreIntoMeter(double km) {
        return m = km / 0.001;
    }

    public double convertMileIntoMeter(double mile) {
        return m = mile / 0.0006214;
    }

    public double convertNauticalMileIntoMeter(double nauticalMile) {
        return m = nauticalMile / 0.00054;
    }

    public double convertCableIntoMeter(double cable) {
        return m = cable / 0.0054;
    }

    public double convertLeagueIntoMeter(double league) {
        return m = league / 0.00018;
    }

    public double convertFootIntoMeter(double foot) {
        return m = foot / 3.281;
    }
    public double convertYardIntoMeter(double yard) {
        return m = yard / 1.094;
    }

}
