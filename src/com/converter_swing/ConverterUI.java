package com.converter_swing;
import  java.awt.*;
import  java.awt.event.*;
import  javax.swing.*;

public class ConverterUI extends JFrame {


    String admeasurementTime[] = {"min", "hour", "day", "week", "month", "astroYear"};
    String admeasurementWeight[] = {"gram", "carat", "engPound", "pound", "stone", "rusPound"};
    String admeasurementVolume[] = {"m3", "gallon", "pint", "quart", "barrel", "cubicFoot"};
    String admeasurementLenght[] = {"km", "mile", "nauticalMile", "cable", "league", "foot", "yard"};
    String admeasurementTemperature[] = {"Kelvin", "Fahrenheit", "Reaumur", "Romer", "Rankine", "Newton", "Delisle"};


    private JTextField inputTime = new JTextField("0", 8);
    private JTextField inputWeight = new JTextField("0", 8);
    private JTextField inputVolume = new JTextField("0", 8);
    private JTextField inputLenght = new JTextField("0", 8);
    private JTextField inputTemperature = new JTextField("0", 8);


    private JTextField inputSec = new JTextField("0", 8);
    private JTextField inputKg = new JTextField("0", 8);
    private JTextField inputLiter = new JTextField("0", 8);
    private JTextField inputMeter = new JTextField("0", 8);
    private JTextField inputCelsium = new JTextField("0", 8);


    private JButton buttonConvert = new JButton("                            CONVERT                     ");
    private JButton buttonRefresh = new JButton("                            REFRESH                     ");

    private JLabel labelTime = new JLabel("          TIME      ");
    private JLabel labelWeight = new JLabel("          WEIGHT      ");
    private JLabel labelVolume = new JLabel("         VOLUME      ");
    private JLabel labelLength = new JLabel("        LENGTH      ");
    private JLabel labelTemp = new JLabel("         TEMP       ");

    private JLabel labelSec = new JLabel("        (sec)   ");
    private JLabel labelKilogram = new JLabel("                (kilogram)      ");
    private JLabel labelLitr = new JLabel("            (litr)      ");
    private JLabel labelMeter = new JLabel("              (meter)      ");
    private JLabel labelCelsium = new JLabel("            (celsium)       ");

    private JComboBox comboBoxTime = new JComboBox(admeasurementTime);
    private JComboBox comboBoxWeight = new JComboBox(admeasurementWeight);
    private JComboBox comboBoxVolume = new JComboBox(admeasurementVolume);
    private JComboBox comboBoxLenght = new JComboBox(admeasurementLenght);
    private JComboBox comboBoxTemperature = new JComboBox(admeasurementTemperature);

    Service service = new Service();

    public ConverterUI() {
        super("Converting everything in the world");
        this.setBounds(500, 500, 530, 240);
        this.setResizable(false);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        Container container = this.getContentPane();
        container.setBackground(Color.lightGray);
        container.setLayout(new FlowLayout());

        container.add(labelTime);
        container.add(labelWeight);
        container.add(labelVolume);
        container.add(labelLength);
        container.add(labelTemp);

        container.add(labelSec);
        container.add(labelKilogram);
        container.add(labelLitr);
        container.add(labelMeter);
        container.add(labelCelsium);

        container.add(inputSec);
        container.add(inputKg);
        container.add(inputLiter);
        container.add(inputMeter);
        container.add(inputCelsium);

        container.add(comboBoxTime);
        container.add(comboBoxWeight);
        container.add(comboBoxVolume);
        container.add(comboBoxLenght);
        container.add(comboBoxTemperature);

        container.add(inputTime);
        container.add(inputWeight);
        container.add(inputVolume);
        container.add(inputLenght);
        container.add(inputTemperature);

        container.add(buttonConvert);
        container.add(buttonRefresh);

        buttonRefresh.addActionListener(service);
        buttonConvert.addActionListener(service);
    }

    String operationTime;
    String operationWeigh;
    String operationVolume;
    String operationLenght;
    String operationTemperature;

    double result1;
    double result2;
    double numSec1;
    double numTime2;

    double resultKg1;
    double resultWeight2;
    double numKg1;
    double numWeight2;

    double resultLiter1;
    double resultVolume2;
    double numLiter1;
    double numVolume2;

    double resultMeter1;
    double resultLenght2;
    double numMetr1;
    double numLenght;

    double resultCelsius1;
    double resultTemperature2;
    double numCelsius;
    double numTemperature;


    class Service implements ActionListener {

        ServiceTime serviceTime = new ServiceTime();
        ServiceWeigh serviceWeigh = new ServiceWeigh();
        ServiceVolume serviceVolume = new ServiceVolume();
        ServiceLength serviceLength = new ServiceLength();
        ServiceTemperature serviceTemperature = new ServiceTemperature();

        public void actionPerformed(ActionEvent e) {


            if (e.getSource() == buttonRefresh) {
                inputSec.setText("0");
                inputTime.setText("0");
                inputCelsium.setText("0");
                inputKg.setText("0");
                inputLenght.setText("0");
                inputLiter.setText("0");
                inputMeter.setText("0");
                inputVolume.setText("0");
                inputWeight.setText("0");
                inputTemperature.setText("0");

            }

            try {
                if (e.getSource() == buttonConvert) {

                    operationTime = "" + comboBoxTime.getItemAt(comboBoxTime.getSelectedIndex());
                    numSec1 = Double.parseDouble(inputSec.getText());
                    numTime2 = Double.parseDouble(inputTime.getText());

                    switch (operationTime) {
                        case "min":
                            if (numSec1 > 0) {
                                result1 = serviceTime.convertSecIntoMin(numSec1);
                                inputTime.setText("" + result1);
                            }
                            if (numTime2 > 0) {
                                result2 = serviceTime.convertMinIntoSec(numTime2);
                                inputSec.setText("" + result2);
                            }
                            break;
                        case "hour":
                            if (numSec1 > 0) {
                                result1 = serviceTime.convertSecIntoHour(numSec1);
                                inputTime.setText("" + result1);
                            }
                            if (numTime2 > 0) {
                                result2 = serviceTime.convertHourIntoSec(numTime2);
                                inputSec.setText("" + result2);
                            }
                            break;
                        case "day":
                            if (numSec1 > 0) {
                                result1 = serviceTime.convertSecIntoDay(numSec1);
                                inputTime.setText("" + result1);
                            }
                            if (numTime2 > 0) {
                                result2 = serviceTime.convertDayIntoSec(numTime2);
                                inputSec.setText("" + result2);
                            }
                            break;
                        case "week":
                            if (numSec1 > 0) {
                                result1 = serviceTime.convertSecIntoWeek(numSec1);
                                inputTime.setText("" + result1);
                            }
                            if (numTime2 > 0) {
                                result2 = serviceTime.convertWeekIntoSec(numTime2);
                                inputSec.setText("" + result2);
                            }
                            break;
                        case "month":
                            if (numSec1 > 0) {
                                result1 = serviceTime.convertSecIntoMonth(numSec1);
                                inputTime.setText("" + result1);
                            }
                            if (numTime2 > 0) {
                                result2 = serviceTime.convertMonthIntoSec(numTime2);
                                inputSec.setText("" + result2);
                            }
                            break;
                        case "astroYear":
                            if (numSec1 > 0) {
                                result1 = serviceTime.convertSecIntoAstronomicalYear(numSec1);
                                inputTime.setText("" + result1);
                            }
                            if (numTime2 > 0) {
                                result2 = serviceTime.convertAstronomicalYearIntoSec(numTime2);
                                inputSec.setText("" + result2);
                            }
                            break;
                    }
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    operationWeigh = "" + comboBoxWeight.getItemAt(comboBoxWeight.getSelectedIndex());
                    numKg1 = Double.parseDouble(inputKg.getText());
                    numWeight2 = Double.parseDouble(inputWeight.getText());

                    switch (operationWeigh) {
                        case "gram":
                            if (numKg1 > 0) {
                                resultKg1 = serviceWeigh.convertKilogramIntoGram(numKg1);
                                inputWeight.setText("" + resultKg1);
                            }
                            if (numWeight2 > 0) {
                                resultWeight2 = serviceWeigh.convertGramIntoKilogram(numWeight2);
                                inputKg.setText("" + resultWeight2);
                            }
                            break;
                        case "carat":
                            if (numKg1 > 0) {
                                resultKg1 = serviceWeigh.convertKilogramCarat(numKg1);
                                inputWeight.setText("" + resultKg1);
                            }
                            if (numWeight2 > 0) {
                                resultWeight2 = serviceWeigh.convertCaratKilogram(numWeight2);
                                inputKg.setText("" + resultWeight2);
                            }
                            break;
                        case "engPound":
                            if (numKg1 > 0) {
                                resultKg1 = serviceWeigh.convertKilogramIntoEngPound(numKg1);
                                inputWeight.setText("" + resultKg1);
                            }
                            if (numWeight2 > 0) {
                                resultWeight2 = serviceWeigh.convertEngPoundIntoKilogram(numWeight2);
                                inputKg.setText("" + resultWeight2);
                            }
                            break;
                        case "pound":
                            if (numKg1 > 0) {
                                resultKg1 = serviceWeigh.convertKilogramIntoPound(numKg1);
                                inputWeight.setText("" + resultKg1);
                            }
                            if (numWeight2 > 0) {
                                resultWeight2 = serviceWeigh.convertPoundIntoKilogram(numWeight2);
                                inputKg.setText("" + resultWeight2);
                            }
                            break;
                        case "stone":
                            if (numKg1 > 0) {
                                resultKg1 = serviceWeigh.convertKilogramIntoStone(numKg1);
                                inputWeight.setText("" + resultKg1);
                            }
                            if (numWeight2 > 0) {
                                resultWeight2 = serviceWeigh.convertStoneIntoKilogram(numWeight2);
                                inputKg.setText("" + resultWeight2);
                            }
                            break;
                        case "rusPound":
                            if (numKg1 > 0) {
                                resultKg1 = serviceWeigh.convertKilogramIntoRusPound(numKg1);
                                inputWeight.setText("" + resultKg1);
                            }
                            if (numWeight2 > 0) {
                                resultWeight2 = serviceWeigh.convertRusPoundIntoKilogram(numWeight2);
                                inputKg.setText("" + resultWeight2);
                            }
                            break;
                    }
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    operationVolume = "" + comboBoxVolume.getItemAt(comboBoxVolume.getSelectedIndex());
                    numLiter1 = Double.parseDouble(inputLiter.getText());
                    numVolume2 = Double.parseDouble(inputVolume.getText());

                    switch (operationVolume) {
                        case "m3":
                            if (numLiter1 > 0) {
                                resultLiter1 = serviceVolume.convertLiterIntoM(numLiter1);
                                inputVolume.setText("" + resultLiter1);
                            }
                            if (numVolume2 > 0) {
                                resultVolume2 = serviceVolume.convertMIntoLiter(numVolume2);
                                inputLiter.setText("" + resultVolume2);
                            }
                            break;
                        case "gallon":
                            if (numLiter1 > 0) {
                                resultLiter1 = serviceVolume.convertLiterIntoGallon(numLiter1);
                                inputVolume.setText("" + resultLiter1);
                            }
                            if (numVolume2 > 0) {
                                resultVolume2 = serviceVolume.convertGallonIntoLiter(numVolume2);
                                inputLiter.setText("" + resultVolume2);
                            }
                            break;
                        case "pint":
                            if (numLiter1 > 0) {
                                resultLiter1 = serviceVolume.convertLiterIntoPint(numLiter1);
                                inputVolume.setText("" + resultLiter1);
                            }
                            if (numVolume2 > 0) {
                                resultVolume2 = serviceVolume.convertPintIntoLiter(numVolume2);
                                inputLiter.setText("" + resultVolume2);
                            }
                            break;
                        case "quart":
                            if (numLiter1 > 0) {
                                resultLiter1 = serviceVolume.convertLiterIntoQuart(numLiter1);
                                inputVolume.setText("" + resultLiter1);
                            }
                            if (numVolume2 > 0) {
                                resultVolume2 = serviceVolume.convertQuartIntoLiter(numVolume2);
                                inputLiter.setText("" + resultVolume2);
                            }
                            break;
                        case "barrel":
                            if (numLiter1 > 0) {
                                resultLiter1 = serviceVolume.convertLiterIntoBarrel(numLiter1);
                                inputVolume.setText("" + resultLiter1);
                            }
                            if (numVolume2 > 0) {
                                resultVolume2 = serviceVolume.convertBarrelIntoLiter(numVolume2);
                                inputLiter.setText("" + resultVolume2);
                            }
                            break;
                        case "cubicFoot":
                            if (numLiter1 > 0) {
                                resultLiter1 = serviceVolume.convertLiterIntoCubicFoot(numLiter1);
                                inputVolume.setText("" + resultLiter1);
                            }
                            if (numVolume2 > 0) {
                                resultVolume2 = serviceVolume.convertCubicFootIntoLiter(numVolume2);
                                inputLiter.setText("" + resultVolume2);
                            }
                            break;
                    }

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


                    operationLenght = "" + comboBoxLenght.getItemAt(comboBoxLenght.getSelectedIndex());
                    numMetr1 = Double.parseDouble(inputMeter.getText());
                    numLenght = Double.parseDouble(inputLenght.getText());


                    switch (operationLenght) {
                        case "km":
                            if (numMetr1 > 0) {
                                resultMeter1 = serviceLength.convertMeterIntoKilometre(numMetr1);
                                inputLenght.setText("" + resultMeter1);
                            }
                            if (numLenght > 0) {
                                resultLenght2 = serviceLength.convertKilometreIntoMeter(numLenght);
                                inputMeter.setText("" + resultLenght2);
                            }
                            break;
                        case "mile":
                            if (numMetr1 > 0) {
                                resultMeter1 = serviceLength.convertMeterIntoMile(numMetr1);
                                inputLenght.setText("" + resultMeter1);
                            }
                            if (numLenght > 0) {
                                resultLenght2 = serviceLength.convertMileIntoMeter(numLenght);
                                inputMeter.setText("" + resultLenght2);
                            }
                            break;
                        case "nauticalMile":
                            if (numMetr1 > 0) {
                                resultMeter1 = serviceLength.convertMeterIntoNauticalMile(numMetr1);
                                inputLenght.setText("" + resultMeter1);
                            }
                            if (numLenght > 0) {
                                resultLenght2 = serviceLength.convertNauticalMileIntoMeter(numLenght);
                                inputMeter.setText("" + resultLenght2);
                            }
                            break;
                        case "cable":
                            if (numMetr1 > 0) {
                                resultMeter1 = serviceLength.convertMeterIntoCable(numMetr1);
                                inputLenght.setText("" + resultMeter1);
                            }
                            if (numLenght > 0) {
                                resultLenght2 = serviceLength.convertCableIntoMeter(numLenght);
                                inputMeter.setText("" + resultLenght2);
                            }
                        case "league":
                            if (numMetr1 > 0) {
                                resultMeter1 = serviceLength.convertMeterIntoLeague(numMetr1);
                                inputLenght.setText("" + resultMeter1);
                            }
                            if (numLenght > 0) {
                                resultLenght2 = serviceLength.convertLeagueIntoMeter(numLenght);
                                inputMeter.setText("" + resultLenght2);
                            }
                        case "foot":
                            if (numMetr1 > 0) {
                                resultMeter1 = serviceLength.convertMeterIntoFoot(numMetr1);
                                inputLenght.setText("" + resultMeter1);
                            }
                            if (numLenght > 0) {
                                resultLenght2 = serviceLength.convertFootIntoMeter(numLenght);
                                inputMeter.setText("" + resultLenght2);
                            }
                            break;
                        case "yard":
                            if (numMetr1 > 0) {
                                resultMeter1 = serviceLength.convertMeterIntoYard(numMetr1);
                                inputLenght.setText("" + resultMeter1);
                            }
                            if (numLenght > 0) {
                                resultLenght2 = serviceLength.convertYardIntoMeter(numLenght);
                                inputMeter.setText("" + resultLenght2);
                            }
                            break;
                    }

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    operationTemperature = "" + comboBoxTemperature.getItemAt(comboBoxTemperature.getSelectedIndex());
                    numCelsius = Double.parseDouble(inputCelsium.getText());
                    numTemperature = Double.parseDouble(inputTemperature.getText());


                    switch (operationTemperature) {
                        case "Kelvin":
                            if (numCelsius > 0) {
                                resultCelsius1 = serviceTemperature.convertCelsiusIntoKelvin(numCelsius);
                                inputTemperature.setText("" + resultCelsius1);
                            }
                            if (numTemperature > 0) {
                                resultTemperature2 = serviceTemperature.convertKelvinIntoCelsius(numTemperature);
                                inputCelsium.setText("" + resultTemperature2);
                            }
                            break;
                        case "Fahrenheit":
                            if (numCelsius > 0) {
                                resultCelsius1 = serviceTemperature.convertCelsiusIntoFahrenheit(numCelsius);
                                inputTemperature.setText("" + resultCelsius1);
                            }
                            if (numTemperature > 0) {
                                resultTemperature2 = serviceTemperature.convertFahrenheitIntoCelsius(numTemperature);
                                inputCelsium.setText("" + resultTemperature2);
                            }
                            break;
                        case "Reaumur":
                            if (numCelsius > 0) {
                                resultCelsius1 = serviceTemperature.convertCelsiusIntoReaumur(numCelsius);
                                inputTemperature.setText("" + resultCelsius1);
                            }
                            if (numTemperature > 0) {
                                resultTemperature2 = serviceTemperature.convertReaumurIntoCelsius(numTemperature);
                                inputCelsium.setText("" + resultTemperature2);
                            }
                        case "Romer":
                            if (numCelsius > 0) {
                                resultCelsius1 = serviceTemperature.convertCelsiusIntoRomer(numCelsius);
                                inputTemperature.setText("" + resultCelsius1);
                            }
                            if (numTemperature > 0) {
                                resultTemperature2 = serviceTemperature.convertRomerIntoCelsius(numTemperature);
                                inputCelsium.setText("" + resultTemperature2);
                            }
                        case "Rankine":
                            if (numCelsius > 0) {
                                resultCelsius1 = serviceTemperature.convertCelsiusIntoRankine(numCelsius);
                                inputTemperature.setText("" + resultCelsius1);
                            }
                            if (numTemperature > 0) {
                                resultTemperature2 = serviceTemperature.convertRankineeIntoCelsius(numTemperature);
                                inputCelsium.setText("" + resultTemperature2);
                            }
                            break;
                        case "Newton":
                            if (numCelsius > 0) {
                                resultCelsius1 = serviceTemperature.convertCelsiusIntNewton(numCelsius);
                                inputTemperature.setText("" + resultCelsius1);
                            }
                            if (numTemperature > 0) {
                                resultTemperature2 = serviceTemperature.convertNewtonIntoCelsius(numTemperature);
                                inputCelsium.setText("" + resultTemperature2);
                            }
                            break;
                        case "Delisle":
                            if (numCelsius > 0) {
                                resultCelsius1 = serviceTemperature.convertCelsiusIntoDelisle(numCelsius);
                                inputTemperature.setText("" + resultCelsius1);
                            }
                            if (numTemperature > 0) {
                                resultTemperature2 = serviceTemperature.convertDelisleIntoCelsius(numTemperature);
                                inputCelsium.setText("" + resultTemperature2);
                            }
                            break;
                    }
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, "Введите в поле число");
            }
        }
    }
}


