package com.converter_swing;

public class ServiceWeigh {

    double kg = 0;
    double g = 0;
    double carat = 0;
    double engPound = 0;
    double pound = 0;
    double stone = 0;
    double rusPound = 0;

    public double convertKilogramIntoGram(double kg) {
        return g = kg * 1000;
    }

    public double convertKilogramCarat(double kg) {
        return carat = kg * 5000;
    }

    public double convertKilogramIntoEngPound(double kg) {
        return engPound = kg * 2.68;
    }

    public double convertKilogramIntoPound(double kg) {
        return pound = kg * 2.2;
    }

    public double convertKilogramIntoStone(double kg) {
        return stone = kg * 0.16;
    }

    public double convertKilogramIntoRusPound(double kg) {
        return rusPound = kg * 16.38;
    }

    ////////////////////////////////////////////////////////////////////

    public double convertGramIntoKilogram(double g) {
        return kg = g / 1000;
    }

    public double convertCaratKilogram(double carat) {
        return kg = carat / 5000;
    }

    public double convertEngPoundIntoKilogram(double EngPound) {
        return kg = EngPound / 2.68;
    }

    public double convertPoundIntoKilogram(double Pound) {
        return kg = Pound / 2.2;
    }

    public double convertStoneIntoKilogram(double Stone) {
        return kg = Stone / 0.16;
    }

    public double convertRusPoundIntoKilogram(double RusPound) {
        return kg = RusPound / 16.38;
    }
}
